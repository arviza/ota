/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100125
 Source Host           : localhost:3306
 Source Schema         : db_ota

 Target Server Type    : MySQL
 Target Server Version : 100125
 File Encoding         : 65001

 Date: 29/03/2019 20:56:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for hotel
-- ----------------------------
DROP TABLE IF EXISTS `hotel`;
CREATE TABLE `hotel`  (
  `id_hotel` int(10) NOT NULL AUTO_INCREMENT,
  `id_kota` int(5) NULL DEFAULT NULL,
  `hotel_nama` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `hotel_alamat` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `hotel_telepon` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `hotel_harga` int(12) NULL DEFAULT NULL,
  `hotel_gambar` longblob NULL,
  PRIMARY KEY (`id_hotel`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of hotel
-- ----------------------------
INSERT INTO `hotel` VALUES (1, 19, 'Burgen', 'Jl. Arjosari Kav.92', '0334-2212422', 200000, NULL);
INSERT INTO `hotel` VALUES (2, 21, 'Mariot', 'ASSA', '033', 250000, NULL);

-- ----------------------------
-- Table structure for kota
-- ----------------------------
DROP TABLE IF EXISTS `kota`;
CREATE TABLE `kota`  (
  `id_kota` int(5) NOT NULL AUTO_INCREMENT,
  `id_provinsi` int(5) NULL DEFAULT NULL,
  `kota_nama` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id_kota`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of kota
-- ----------------------------
INSERT INTO `kota` VALUES (1, 1, 'Singaraja');
INSERT INTO `kota` VALUES (2, 1, 'Gianyar');
INSERT INTO `kota` VALUES (3, 1, 'Denpasar');
INSERT INTO `kota` VALUES (4, 2, 'Jakarta');
INSERT INTO `kota` VALUES (9, 3, 'Bandung');
INSERT INTO `kota` VALUES (10, 3, 'Bekasi');
INSERT INTO `kota` VALUES (11, 3, 'Bogor');
INSERT INTO `kota` VALUES (12, 3, 'Cianjur');
INSERT INTO `kota` VALUES (13, 3, 'Cirebon');
INSERT INTO `kota` VALUES (14, 4, 'Pekalongan');
INSERT INTO `kota` VALUES (15, 4, 'Semarang');
INSERT INTO `kota` VALUES (16, 5, 'Banyuwangi');
INSERT INTO `kota` VALUES (17, 5, 'Blitar');
INSERT INTO `kota` VALUES (18, 5, 'Jember');
INSERT INTO `kota` VALUES (19, 5, 'Malang');
INSERT INTO `kota` VALUES (20, 5, 'Pasuruan');
INSERT INTO `kota` VALUES (21, 5, 'Surabaya');

-- ----------------------------
-- Table structure for provinsi
-- ----------------------------
DROP TABLE IF EXISTS `provinsi`;
CREATE TABLE `provinsi`  (
  `id_provinsi` int(5) NOT NULL AUTO_INCREMENT,
  `provinsi_nama` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id_provinsi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of provinsi
-- ----------------------------
INSERT INTO `provinsi` VALUES (1, 'Bali');
INSERT INTO `provinsi` VALUES (2, 'Jakarta');
INSERT INTO `provinsi` VALUES (3, 'Jawa Barat');
INSERT INTO `provinsi` VALUES (4, ' Jawa Tengah');
INSERT INTO `provinsi` VALUES (5, ' Jawa Timur');
INSERT INTO `provinsi` VALUES (6, 'Kalimantan');
INSERT INTO `provinsi` VALUES (11, ' Yogyakarta');

SET FOREIGN_KEY_CHECKS = 1;
