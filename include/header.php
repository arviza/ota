<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Online Travel Agent</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/all.css">
    <link rel="stylesheet" href="css/fontawesome-stars.css">
    <link rel="stylesheet" href="css/datepicker.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <section class="header bg-primary">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-dark ">
                <a class="navbar-brand" href="#"><i class="fab fa-ravelry"></i> Ravelry</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">My Booking </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Help Center </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/demo/ota/contact.php">Contact Us</a>
                        </li>
                    </ul>
                </div>
                <div class="login-account mr-sm-2 d-none d-sm-none d-md-none d-lg-block">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="#">Sign Up</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Login
                            </a>
                            <div class="dropdown-menu dropdown-login" aria-labelledby="navbarDropdownMenuLink">
                                <!--                                <div class="login-form">-->
                                <form action="#" method="post">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Username" required="required">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" placeholder="Password" required="required">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-block">Log in</button>
                                    </div>
                                    <div class="clearfix">
                                        <label class="pull-left checkbox-inline"><input type="checkbox"> Remember me</label> <br>
                                        <a href="#" class="pull-right">Forgot Password?</a>
                                    </div>
                                </form>
                            </div>
                            <!--                            </div>-->
                        </li>
                    </ul>
                </div>
            </nav>

            <!--            <nav class="navbar navbar-dark bg-primary">
                <a class="navbar-brand" href="#">Navbar</a>

                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">My Booking </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Help Center </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Contact Us</a>
                        </li>



                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </form>

            </nav>-->
        </div>
    </section>
