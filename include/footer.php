<!-- Footer -->
<!-- Footer Links -->
    <div class="container-fluid bg-primary">

        <!-- Grid row-->
        <div class="row py-4 d-flex align-items-center">

            <!-- Grid column -->
            <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
                <h6 class="mb-0">Follow kami di sosial media!</h6>
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-6 col-lg-7 text-center text-md-right">

                <!-- Facebook -->
                <a class="fb-ic">
                    <i class="fab fa-facebook-f white-text mr-4"> </i>
                </a>
                <!-- Twitter -->
                <a class="tw-ic">
                    <i class="fab fa-twitter white-text mr-4"> </i>
                </a>
                <!-- Google +-->
                <a class="gplus-ic">
                    <i class="fab fa-google-plus-g white-text mr-4"> </i>
                </a>
                <!--Linkedin -->
                <a class="li-ic">
                    <i class="fab fa-linkedin-in white-text mr-4"> </i>
                </a>
                <!--Instagram-->
                <a class="ins-ic">
                    <i class="fab fa-instagram white-text"> </i>
                </a>

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row-->

    </div>
<footer class="page-footer font-small unique-color-dark">





    <!-- Footer Links -->
    <div class="container text-center text-md-left mt-5">

        <!-- Grid row -->
        <div class="row mt-3">

            <!-- Grid column -->
            <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">

                <!-- Content -->
                <h6 class="text-uppercase font-weight-bold">Tentang kami</h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto text-center" style="width: 60px;">
                <p>Kami merupakan Online Travel Agent terpercaya yang menjual berbagai tiket pesawat, hotel dan tiket kereta untuk menjamin kenyamanan Anda dalam liburan dengan harga terbaik.</p>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">

                <!-- Links -->
                <h6 class="text-uppercase font-weight-bold">Legal and Partner</h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p>
                    <a href="#!">Terms and Conditions</a>
                </p>
                <p>
                    <a href="#!">Privacy Policy</a>
                </p>
                <p>
                    <a href="#!">Register your Hotels and Flights</a>
                </p>
                <p>
                    <a href="#!">Partner Program</a>
                </p>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">

                <!-- Links -->
                <h6 class="text-uppercase font-weight-bold">Useful links</h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p>
                    <a href="#!">How To Book</a>
                </p>
                <p>
                    <a href="#!">Discount Page</a>
                </p>
                <p>
                    <a href="#!">Help Center</a>
                </p>
                <p>
                    <a href="#!">About Us</a>
                </p>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

                <!-- Links -->
                <h6 class="text-uppercase font-weight-bold">Kontak kami</h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p>
                    <i class="fas fa-home mr-3"></i> Jakarta, ID 15419, ID</p>
                <p>
                    <i class="fas fa-envelope mr-3"></i> info@example.com</p>
                <p>
                    <i class="fas fa-phone mr-3"></i> + 01 234 567 88</p>
                <p>
                    <i class="fas fa-print mr-3"></i> + 01 234 567 89</p>

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </div>
 <!-- Grid column -->
             <div class="container-fluid bg-secondary">

        <!-- Grid row-->
        <div class="row py-4 d-flex align-items-center">

            <!-- Grid column -->
            <div class="col-md-12 col-lg-12 text-center text-md-center mb-12 mb-md-0">
                <h6 class="mb-0">Create Your own Online Travel Agent through our Partner Program</h6>
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-6 col-lg-7 text-center text-md-right">



            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row-->

    </div>

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2019 Copyright:
        <a href="https://mdbootstrap.com/education/bootstrap/"> Ravelry</a>
    </div>
    <!-- Copyright -->

</footer>
<!-- Footer -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.autocomplete.min.js"></script>
<script src="js/datepicker.min.js"></script>
<script src="js/datepicker.en.js"></script>
<script src="js/jquery.barrating.min.js"></script>
<script src="js/popper.min.js"></script>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet"/>

<script type="text/javascript">
    $('#minDate').datepicker({
        dateFormat: 'dd/mm/yy ',
          minDate: +0
    });
    $(document).ready(function() {
        $("button.swap").click(function() {
            $('#from').val([$('#to').val(), $('#to').val($('#from').val())][0])
        });
         // $('#minDate').html(new Date());
    });
    $(function() {
        $('#bintang').barrating({
            theme: 'fontawesome-stars'
        });
    });

</script>

</body>

</html>
