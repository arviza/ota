<?php include 'include/header.php' ?>

<section class="menu-tab">
    <div class="container">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pesawat-tab" data-toggle="tab" href="#pesawat" role="tab">
                    <i class="fas fa-plane-departure"></i> Pesawat
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="hotel-tab" data-toggle="tab" href="#hotel" role="tab">
                    <i class="fas fa-hotel"></i> Hotel
                </a>
            </li>
<!--
            <li class="nav-item">
                <a class="nav-link" id="kereta-tab" data-toggle="tab" href="#kereta" role="tab">
                    <i class="fas fa-subway"></i> Kereta
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="bis-tab" data-toggle="tab" href="#bis" role="tab">
                    <i class="fas fa-bus"></i> Bis
                </a>
            </li>
-->
        </ul>
    </div>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="pesawat" role="tabpanel">
            <div class="bg-pesawat"></div>
            <div class="container">
                <form action="#" method="post" enctype="multipart/form-data">
                    <p class="pilihan"><input name="tipe" id="pil1" type="radio" checked> <label for="pil1">Pulang Pergi</label></p>
                    <p class="pilihan"><input name="tipe" id="pil2" type="radio"> <label for="pil2">Sekali Jalan</label></p>

                    <div class="input-group" id="pilihan-pulang">
                        <input type="text" id="from" name="from" placeholder="Kota Awal">
                        <input type="text" id="to" name="to" placeholder="Kota Tujuan">
                        <button type="button" class="swap"><i class="fas fa-exchange-alt"></i></button>
                        <input type="text" placeholder="Tanggal Keberangkatan" id="minDate" class='datepicker-here' data-language='en'>
                        <input type="text" placeholder="Tanggal Kedatangan" class='datepicker-here' data-language='en'>
                        <input type="text" placeholder="Kelas Kabin">
                    </div>
                    <div class="input-group" id="pilihan-pulang">
                        
                    </div>
                    <div class="input-group" id="pilihan-sekali" style="display: none">
                        <input type="text" placeholder="Negara Awal">
                        <input type="text" placeholder="Negara Tujuan">
                        <button><i class="fas fa-exchange-alt"></i></button>
                        <input type="text" placeholder="Tanggal Keberangkatan">
                        <input type="text" placeholder="Kelas Kabin">
                    </div>

                    <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> Cari Penerbangan</button>
                    <button type="reset" class="btn btn-danger"><i class="fas fa-times"></i> Bersihkan</button>
                </form>
            </div>
        </div>
        <div class="tab-pane fade" id="hotel" role="tabpanel">
            <div class="bg-hotel"></div>
            <div class="container">
                <form action="#" method="post" enctype="multipart/form-data">
                    <h3 class="judul"><i class="fas fa-bed"></i> Booking Hotel Murah Online dengan Harga Promo</h3>
                    <div class="row">
                        <div class="col-6">
                            <input type="text" id="kota" class="form-control" name="kota" placeholder="Kota Tujuan">
                            <small class="text-muted">Masukkan tujuan kota anda</small>
                        </div>
                        <div class="col-6">
                            <input type="text" id="kota" class="form-control" name="kota" placeholder="Tujuan">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <input type="text" class="form-control" style="width:49.7%; display:inline-block" placeholder="Check In" id="minDate" class='datepicker-here' data-language='en'>
                            <input type="text" class="form-control" style="width:49.5%; display:inline-block" placeholder="Check Out" id="minDate" class='datepicker-here' data-language='en'>
                        </div>
                        <div class="col-6">
                            <label>Bintang</label>
                            <select id="bintang">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> Cari Penerbangan</button>
                    <button type="reset" class="btn btn-danger"><i class="fas fa-times"></i> Bersihkan</button>
                </form>
            </div>
        </div>
        <div class="tab-pane fade" id="kereta" role="tabpanel">
            <div class="bg-kereta"></div>
            <div class="container">
                <div class="col-*-3 right">
                    <p>Check-in</p>
                    <input type='text' class='datepicker-here' data-language='en' />
                </div>
                <div class="col-*-3 right">
                    <p>Check-out</p>
                    <input type='text' class='datepicker-here' data-language='en' />
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="bis" role="tabpanel">
            <div class="bg-bis"></div>
            <div class="container">

                <p>Berangkat</p>
                <input type='text' class='datepicker-here' data-language='en' />

                <div class="row">
                    <div class="col-4">
                        <div class="input-group" id="pilihan-hotel">
                            <input type="text" placeholder="Nama kota">
                            <input type="text" placeholder="Check-in" class='datepicker-here' data-language='en'>
                            <input type="text" placeholder="Check-out" class='datepicker-here' data-language='en'>
                            <input type="text" placeholder="Tamu dan Kamar">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="input-group" id="pilihan-kereta">
                            <input type="text" placeholder="Nama Stasiun">
                            <input type="text" placeholder="Jadwal Keberangkatan" class='datepicker-here' data-language='en'>
                            <input type="text" placeholder="Jumlah Penumpang dan Kelas">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="input-group" id="pilihan-bis">
                            <input type="text" placeholder="Nama Terminal">
                            <input type="text" placeholder="Jadwal Keberangkatan" class='datepicker-here' data-language='en'>
                            <input type="text" placeholder="Jumlah Penumpang dan Kelas">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="container-fluid">
    <div class="fitur">
        <div class="container" align="center">
            <p class="judul-fitur">
                <h1>3 Langkah mudah Merencanakan Liburan anda </h1>
            </p>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="card">
                        <i class="fas fa-search fa-7x" id="fitur-3-langkah"></i>
                        <div class="card-body">
                            <h4 class="card-title">Cari Akomodasi Anda</h4>
                            <p class="card-text">Situs kami menjual Berbagai Tiket Pesawat, Hotel dan kereta dengan harga yang kompetitif</p>
                        </div>
                    </div>
                    <br>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="card">
                        <i class="far fa-hand-point-up fa-7x" id="fitur-3-langkah"></i>
                        <div class="card-body">
                            <h4 class="card-title">Pilih Sesuai Selera Anda</h4>
                            <p class="card-text">Ekonomi atau Bisnis?, Deluxe Rooom atau Suite Room? Kami punya yang anda mau</p>
                        </div>
                    </div>
                    <br>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="card">
                        <i class="fas fa-file-invoice-dollar fa-7x" id="fitur-3-langkah"></i>
                        <div class="card-body">
                            <h4 class="card-title">Nikmati penerbangan anda</h4>
                            <p class="card-text">Dengan One Page user details and Payment, Pemesanan akomodasi jadi lebih mudah</p>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>

</section>

<?php include 'include/footer.php' ?>
