<?php include 'include/header.php' ?>

<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah List Hotel</h4>
            <form class="forms-sample" action="proses.php?hotel=tambah" enctype="multipart/form-data" method="post">
                <div class="form-group">
                    <label>Kota</label>
                    <select class="form-control" name="kota">
                        <?php
                            $query = mysql_query("SELECT * FROM kota k JOIN provinsi p ON k.id_provinsi=p.id_provinsi ORDER BY k.id_kota ASC");
                            while($data = mysql_fetch_array($query)){
                        ?>
                        <option value="<?= $data['id_kota'] ?>"><?= $data['provinsi_nama']." - ".$data['kota_nama'] ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Nama Hotel</label>
                    <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama Hotel">
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label>Telepon</label>
                            <input type="text" class="form-control" name="telepon" placeholder="Masukkan Telepon">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label>Harga</label>
                            <input type="text" name="harga" class="form-control" placeholder="Masukkan Harga">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Gambar Hotel</label>
                    <input type="file" name="gambar" class="file-upload-default">
                    <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled="" placeholder="Gambar Hotel">
                        <span class="input-group-append">
                            <button class="file-upload-browse btn btn-primary" type="button">Pilih Gambar</button>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label>Alamat Lokasi</label>
                    <textarea class="form-control" name="alamat" rows="5"></textarea>
                </div>
                <button type="submit" class="btn btn-primary mr-2">Tambah</button>
                <button class="btn btn-light">Batal</button>
            </form>
        </div>
    </div>
</div>

<?php include 'include/footer.php' ?>
