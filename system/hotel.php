<?php include 'include/header.php' ?>

<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <a href="hotel-tambah.php" class="btn btn-primary btn-rounded btn-icon icon-link">
                <span class="mdi mdi-plus"></span>
            </a>
            <a href="hotel-tambah.php" class="btn btn-info btn-rounded btn-icon icon-link">
                <span class="mdi mdi-upload"></span>
            </a>
            <h4 class="card-title">List Hotel</h4>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>
                            Pict
                        </th>
                        <th>
                            Kota
                        </th>
                        <th>
                            Nama Hotel
                        </th>
                        <th>
                            Telepon
                        </th>
                        <th>
                            Harga Permalam
                        </th>
                        <th>
                            Alamat
                        </th>
                        <th>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                            $no = 1;
                            $query = mysql_query("SELECT * FROM hotel h JOIN kota k ON h.id_kota=k.id_kota ORDER BY h.id_hotel ASC");
                            while ($data = mysql_fetch_array($query)){
                        ?>
                    <tr>
                        <td>
                            <span class="mdi mdi-hotel"></span>
                        </td>
                        <td>
                            <?= $data['kota_nama'] ?>
                        </td>
                        <td>
                            <?= $data['hotel_nama'] ?>
                        </td>
                        <td>
                            <?= $data['hotel_telepon'] ?>
                        </td>
                        <td>
                            Rp.<?= number_format($data['hotel_harga'],0); ?>
                        </td>
                        <td>
                            <?= $data['hotel_alamat'] ?>
                        </td>
                        <td>
                            <a href="#" class="btn btn-outline-primary btn-rounded btn-icon icon-link-2">
                                <i class="mdi mdi-lead-pencil"></i>
                            </a>
                            <a href="#" class="btn btn-outline-danger btn-rounded btn-icon icon-link-2">
                                <i class="mdi mdi-delete"></i>
                            </a>
                        </td>
                    </tr>
                    <?php $no++; } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php include 'include/footer.php' ?>
