<?php include 'include/header.php' ?>

<div class="row">
    <div class="col-lg-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Provinsi</h4>
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="10%" class="text-center">#</th>
                                <th width="60%">Nama Provinsi</th>
                                <th width="30%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                    $no = 1;
                                    $query = mysql_query("SELECT * FROM provinsi ORDER BY provinsi_nama ASC");
                                    while ($data = mysql_fetch_array($query)){
                                ?>
                            <tr>
                                <td class="py-1 text-center">
                                    <?= $no ?>.
                                </td>
                                <td>
                                    <?= $data['provinsi_nama'] ?>
                                </td>
                                <td>
                                    <button class="btn btn-danger btn-rounded btn-icon" title="Ubah">
                                        <span class="mdi mdi-lead-pencil"></span>
                                    </button>
                                    <button class="btn btn-primary btn-rounded btn-icon" title="List Hotel">
                                        <span class="mdi mdi-arrow-right"></span>
                                    </button>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Kota</h4>
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="10%" class="text-center">#</th>
                                <th width="60%">Nama Provinsi</th>
                                <th width="30%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $no = 1;
                                $query = mysql_query("SELECT * FROM kota ORDER BY kota_nama ASC");
                                while ($data = mysql_fetch_array($query)){
                            ?>
                            <tr>
                                <td class="py-1 text-center">
                                    <?= $no ?>.
                                </td>
                                <td>
                                    <?= $data['kota_nama'] ?>
                                </td>
                                <td>
                                    <button class="btn btn-danger btn-rounded btn-icon" title="Ubah">
                                        <span class="mdi mdi-lead-pencil"></span>
                                    </button>
                                    <button class="btn btn-primary btn-rounded btn-icon" title="List Hotel">
                                        <span class="mdi mdi-arrow-right"></span>
                                    </button>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'include/footer.php' ?>
