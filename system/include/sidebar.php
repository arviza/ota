
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href=".">
                <i class="mdi mdi-home menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#pesawat">
                <i class="mdi mdi-airplane menu-icon"></i>
                <span class="menu-title">Pesawat</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="pesawat">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="#">List Data</a></li>
                    <li class="nav-item"> <a class="nav-link" href="#">Area</a></li>
                    <li class="nav-item"> <a class="nav-link" href="#">Kategori</a></li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#hotel">
                <i class="mdi mdi-hotel menu-icon"></i>
                <span class="menu-title">Hotel</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="hotel">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="hotel.php">List Data</a></li>
                    <li class="nav-item"> <a class="nav-link" href="hotel-area.php">Area</a></li>
                    <li class="nav-item"> <a class="nav-link" href="#">Kategori</a></li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#bis">
                <i class="mdi mdi-bus menu-icon"></i>
                <span class="menu-title">Bis</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="bis">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="#">List Data</a></li>
                    <li class="nav-item"> <a class="nav-link" href="#">Area</a></li>
                    <li class="nav-item"> <a class="nav-link" href="#">Kategori</a></li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#kereta">
                <i class="mdi mdi-train menu-icon"></i>
                <span class="menu-title">Kereta</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="kereta">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="#">List Data</a></li>
                    <li class="nav-item"> <a class="nav-link" href="#">Area</a></li>
                    <li class="nav-item"> <a class="nav-link" href="#">Kategori</a></li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="pages/icons/mdi.html">
                <i class="mdi mdi-playlist-check menu-icon"></i>
                <span class="menu-title">Pemesanan</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="pages/icons/mdi.html">
                <i class="mdi mdi-file-document-box-outline menu-icon"></i>
                <span class="menu-title">Laporan</span>
            </a>
        </li>
    </ul>
</nav>
