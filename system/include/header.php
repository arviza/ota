<?php
    error_reporting(0);
    include 'config.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Online Travel Agent</title>

    <link rel="stylesheet" href="vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="vendors/base/vendor.bundle.base.css">
    <link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
    <link rel="stylesheet" href="css/materialdesignicons.min.css">
    <link rel="stylesheet" href="css/style.css">

    <link rel="stylesheet" href="../css/all.css">
    <link rel="stylesheet" href="../css/datepicker.min.css">
    <link rel="stylesheet" href="../css/system.css">
</head>

<body>
    <div class="container-scroller">

        <?php include 'include/navbar.php' ?>

        <div class="container-fluid page-body-wrapper">

            <?php include 'include/sidebar.php' ?>

            <div class="main-panel">

                <div class="content-wrapper">
